import Grid from './grid.js';
import Piece from './piece.js';

export default class Game {
  constructor() {
    this.score = 0;
    this.level = 0;
    this.lines = 0;
    this.gameOver = false;
    this.grid = new Grid(20, 10);
    this.activePiece = new Piece('T', 'purple', [
      [0,0,0],
      [1,1,1],
      [0,1,0]
    ]);
  }

  movePieceLeft() {
    this.activePiece.x -= 1;
  }

  movePieceRight() {
    this.activePiece.x += 1;
  }

  movePieceDown() {
    this.activePiece.y += 1;
  }
}
