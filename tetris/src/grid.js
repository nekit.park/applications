export default class Grid {
  constructor(rows, columns) {
    this.rows = rows;
    this.columns = columns;

    for (let i = 0; i < rows; i++) {
      this[i] = new Array(columns).fill(0);
    }
  }

  lockPiece(piece) {
    for (let y = 0; y < piece.blocks.length; y++) {
      for (let x = 0; x < piece.blocks[y].length; x++) {
        const block = piece.blocks[y][x];

        this[piece.y + y][piece.x + x] = block;
      }
    }
  }
}