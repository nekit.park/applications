class Board {
  constructor() {
    this.field = [
      [0,0,0],
      [0,0,0],
      [0,0,0]
    ];
    this.activePlayer = 1;
    this.winCombos = [
      [[0,0], [0,1], [0,2]],
      [[1,0], [1,1], [1,2]],
      [[2,0], [2,1], [2,2]],
      [[0,0], [1,0], [2,0]],
      [[0,1], [1,1], [2,1]],
      [[0,2], [1,2], [2,2]],
      [[0,0], [1,1], [2,2]],
      [[2,0], [1,1], [0,2]]
    ]
  }

  click(activePlayer, x, y) {
    this.field[y][x] = this.activePlayer;
    this.activePlayer = (this.activePlayer === 1) ? 2 : 1;
    this.checkWin
  }

  checkWin() {

  }
}

let newBoard = new Board();
console.log(newBoard)